import { WebSocketServer } from 'ws';

// WebSocket Server
const wss1 = new WebSocketServer({ port: 8085 });
const wss2 = new WebSocketServer({ port: 8086 });

let data = {
	chart1: [
		{ data: [65, 59, 80, 81, 56, 55, 40], label: 'First dataset' },
	 	{ data: [28, 48, 40, 19, 86, 27, 90], label: 'Second dataset' },
		{ data: [45, 25, 16, 36, 67, 18, 76], label: 'Third dataset - No bezier' }
	],
	chart2: [
		{ data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
		{ data: [45, 25, 16, 36, 67, 18, 76], label: 'Series B' }
	]
};


wss1.on('connection', ws => {
	console.log('• ws1 connection!');
	// simulate real-time data for second chart
	const generateRandomData = () => {
		data.chart1.map( set => {
			set.data = set.data.map( point => {
				return point = Math.floor(Math.random()*100)
			});
		});
	  	ws.send( JSON.stringify(data.chart1) );
	}
	generateRandomData();

	setInterval( () => {
		generateRandomData();
	}, 5000)

	ws.onopen = () => generateRandomData();
	ws.onerror = (error) => console.log(`ws1 error: ${error['code']}`);
	ws.onclose = () => console.log(`• ws1 onclose!`);
});

wss2.on('connection', ws => {
	console.log('• ws2 connection!');
	// simulate real-time data for second chart
	const generateRandomData = () => {
		data.chart2.map( set => {
			set.data = set.data.map( point => {
				return point = Math.floor(Math.random()*300)
			});
		});
	  	ws.send( JSON.stringify(data.chart2) );
	}
	generateRandomData();

	setInterval( () => {
		generateRandomData();
	}, 4000)

	ws.onopen = () => generateRandomData();
	ws.onerror = (error) => console.log(`ws2 error: ${error['code']}`);
	ws.onclose = () => console.log(`• ws2 onclose!`);
});