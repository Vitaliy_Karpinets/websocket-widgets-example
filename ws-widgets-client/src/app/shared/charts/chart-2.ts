export var areaChartData: Array<any> = [
  { data: [], label: 'Series A' },
  { data: [], label: 'Series B' }
];

export var areaChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

export var areaChartOptions: any = {
  animation: {
    duration: 2000, // general animation time
    easing: 'easeOutBack'
  },
  legend: {
    position: 'bottom',
  },
  hover: {
    mode: 'label',
    animationDuration: 1000, // duration of animations when hovering an item
  },
  responsiveAnimationDuration: 1000, // animation duration after a resize
  responsive: true,
  maintainAspectRatio: false,
  scales: {
    xAxes: [{
      display: true,
      gridLines: {
        color: "#F5F5F5",
        drawTicks: false,
      },
      scaleLabel: {
        display: true,
        labelString: 'Month'
      }
    }],
    yAxes: [{
      display: true,
      gridLines: {
        color: "#F5F5F5",
        drawTicks: false,
      },
      scaleLabel: {
        display: true,
        labelString: 'Value'
      }
    }]
  },
  title: {
    display: true,
    text: 'Chart.js Area Chart - Legend'
  }
};

export var areaChartColors: Array<any> = [
  {

    backgroundColor: "rgba(189, 189, 189, 0.3)",
    borderColor: 'transparent',
    pointBackgroundColor: '#FFF',
    pointBorderColor: '#BDBDBD',
    pointHoverBackgroundColor: 'rgba(255, 141, 96,1)',
    pointRadius: '5',
    pointHoverBorderColor: '#FFF',
    pointHoverRadius: '5',
    pointBorderWidth: '2'
  },
  {
    backgroundColor: "rgba(47, 139, 230, 0.7)",
    borderColor: 'transparent',
    pointBackgroundColor: '#FFF',
    pointBorderColor: '#2F8BE6',
    pointHoverBackgroundColor: '#2F8BE6',
    pointRadius: '5',
    pointHoverBorderColor: '#FFF',
    pointHoverRadius: '5',
    pointBorderWidth: '2'
  },
];

export var areaChartLegend = true;

export var areaChartType = 'line';