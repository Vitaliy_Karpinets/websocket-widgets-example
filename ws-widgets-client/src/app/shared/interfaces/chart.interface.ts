export interface IChart{
    datasets: Array<any>,
    labels: Array<any>,
    options: any,
    colors: Array<any>,
    legend: boolean,
    chartType: string
}