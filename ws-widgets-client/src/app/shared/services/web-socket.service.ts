import { Injectable } from '@angular/core';
import { IChart } from '../interfaces/chart.interface';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  constructor() { }

  public connect(link:string, widget:IChart){
    const ws: WebSocket = new WebSocket(link);
    ws.onmessage = (d:MessageEvent<any>):void => {
      // current data of widget/chart
      widget.datasets = JSON.parse(d.data);
    }
    ws.onclose = () => {
      // reconnect to websocket
      setTimeout(()=>{
        this.connect(link, widget);
      }, 5000);
    };
    ws.onerror = (error) => {
      // handle errors
    };
  }

}
