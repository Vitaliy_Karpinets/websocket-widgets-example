import { Component, OnInit } from '@angular/core';
import * as chart1 from './shared/charts/chart-1';
import * as chart2 from './shared/charts/chart-2';
import { IChart } from './shared/interfaces/chart.interface';
import { WebSocketService } from './shared/services/web-socket.service';

const wslinks = {
  CHART1_WS_LINK: 'ws://localhost:8085',
  CHART2_WS_LINK: 'ws://localhost:8086',
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  chart1:IChart = {
    datasets: chart1.lineChartData,
    labels: chart1.lineChartLabels,
    options: chart1.lineChartOptions,
    colors: chart1.lineChartColors,
    legend: chart1.lineChartLegend,
    chartType: chart1.lineChartType
  };
  chart2:IChart = {
    datasets: chart2.areaChartData,
    labels: chart2.areaChartLabels,
    options: chart2.areaChartOptions,
    colors: chart2.areaChartColors,
    legend: chart2.areaChartLegend,
    chartType: chart2.areaChartType
  };
  // etc.

  constructor( private wsService: WebSocketService ) { }

  ngOnInit(): void {
    this.wsService.connect( wslinks.CHART1_WS_LINK, this.chart1 );
    this.wsService.connect( wslinks.CHART2_WS_LINK, this.chart2 );
    // etc.
  }

  public chartClicked(e: any): void {
    // handle 'click' event of chart
  }

  public chartHovered(e: any): void {
    // handle 'mousemove' event of chart on data points    
  }
}
